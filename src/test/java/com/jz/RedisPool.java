package com.jz;

import redis.clients.jedis.*;

import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class RedisPool {
    private static JedisPool jedisPool;
    static {
        ResourceBundle bundle = ResourceBundle.getBundle("redis");
        if(bundle == null){
            throw new IllegalArgumentException("[redis.properties] is not found");
        }

        JedisPoolConfig config = new JedisPoolConfig();

        config.setMaxTotal(Integer.valueOf(bundle.getString("redis.pool.MaxTotal")));
        config.setMaxIdle(Integer.valueOf(bundle.getString("redis.pool.MaxIdle")));
        config.setMaxWaitMillis(Long.valueOf(bundle.getString("redis.pool.MaxWaitMillis")));

        config.setTestOnBorrow(Boolean.valueOf(bundle.getString("redis.pool.testOnBorrow")));
        config.setTestOnReturn(Boolean.valueOf(bundle.getString("redis.pool.testOnReturn")));

        jedisPool = new JedisPool(config, bundle.getString("redis.ip"), Integer.valueOf(
                bundle.getString("redis.port")));

        Jedis jedis = jedisPool.getResource();
        String keys = "name";
        String value = jedis.get(keys);

        jedis.del(keys);
        jedis.set(keys,"zj");

        System.out.println(value);
        jedisPool.close();

        /**
         * Identical hash
         * */
        JedisShardInfo jedisShardInfo1 = new JedisShardInfo(bundle.getString("redis1.ip"),
                Integer.valueOf(bundle.getString("redis.port")));
        JedisShardInfo jedisShardInfo2 = new JedisShardInfo(bundle.getString("redis2.ip"),
                Integer.valueOf(bundle.getString("redis.port")));

        List<JedisShardInfo> list = new LinkedList<>();
        list.add(jedisShardInfo1);
        list.add(jedisShardInfo2);

        ShardedJedisPool shardedJedisPool = new ShardedJedisPool(config,list);

        ShardedJedis shardedJedis = shardedJedisPool.getResource();

        String keys1 = "name";
        String value1 = "zj";

        shardedJedis.del(keys1);
        shardedJedis.set(keys1,value1);
        System.out.println(shardedJedis.get(keys1));
        shardedJedis.close();
    }
}
